const baseURL = "https://pokeapi.co/api/v2/pokemon";

let pokemonCount = 1154;
let currentPage = 1;
let pokemonPerPage = 30;
let pokedexInnerContainer = document.getElementById("pokedex-inner-container");
let prevButton = document.getElementById("prev-button");
let nextButton = document.getElementById("next-button");

const init = () => {
    getPokedexData()
        .then((json) => {
            generatePokedexDivs(json.results);
        })
        .catch((err) => {
            throw err;
        });
};

const getPokedexData = (offset = 0) => {
    return fetch(`${baseURL}?offset=${offset}&limit=${pokemonPerPage}`).then((response) =>
        response.json()
    );
};

const generatePokedexDivs = async (pokedexData) => {
    pokedexInnerContainer.innerHTML = "";
    for (
        let pokemonIndex = 0;
        pokemonIndex < pokedexData.length;
        pokemonIndex++
    ) {
        const pokemonContainerDiv = document.createElement("div");
        pokemonContainerDiv.classList.add("pokemon-div");
        const pokemonDiv = await generatePokemonDiv(pokedexData[pokemonIndex]);
        pokemonContainerDiv.append(pokemonDiv);
        pokedexInnerContainer.append(pokemonContainerDiv);
    }
};

const getPokemonData = (pokemonName) => {
    return fetch(`${baseURL}/${pokemonName}`).then((response) =>
        response.json().then((json) => {
            return json;
        })
    );
};

const generatePokemonDiv = async (pokemonSearchData) => {
    const pokemonDiv = document.createElement("div");
    const pokemonNameDiv = document.createElement("h2");
    const pokemonData = await getPokemonData(pokemonSearchData.name);
    pokemonNameDiv.innerText = pokemonData.name;
    const pokemonImgSrc =
        pokemonData.sprites.other["official-artwork"].front_default;
    const pokemonImg = document.createElement("img");
    pokemonImg.width = 200;
    pokemonImg.height = 200;
    pokemonImg.src = pokemonImgSrc;
    pokemonDiv.append(pokemonNameDiv, pokemonImg);
    return pokemonDiv;
};

nextButton.addEventListener("click", () => {
    currentPage++;
    if (currentPage === Math.ceil(pokemonCount / pokemonPerPage)) {
        nextButton.disabled = true;
    }
    prevButton.disabled = false;
    getPokedexData((currentPage - 1) * pokemonPerPage)
        .then((json) => {
            generatePokedexDivs(json.results);
        })
        .catch((err) => {
            throw err;
        });
});

prevButton.addEventListener("click", () => {
    currentPage--;
    if (currentPage === 1) {
        prevButton.disabled = true;
    }
    nextButton.disabled = false;
    getPokedexData((currentPage - 1) * pokemonPerPage)
        .then((json) => {
            generatePokedexDivs(json.results);
        })
        .catch((err) => {
            throw err;
        });
});

window.addEventListener("load", init, false);
